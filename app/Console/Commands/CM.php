<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class CM extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cm:make {make : Class (singular) for example Cube}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Private CubeMaster Generator';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('make');

        $this->info('Generating Migration...');
        Artisan::call('make:migration', ['name' => 'create_'.Str::plural(Str::lower($name)).'_table']);

        $this->info('Generating Model...');
        $this->model($name);

        $this->info('Generating Controller...');
        $this->controller($name);

        $this->info('Generating datatable...');
        $this->datatable($name);

        $this->info('Generating views...');
        $this->resource($name, 'index');
        $this->resource($name, 'create');
        $this->resource($name, 'edit');
        $this->resource($name, 'show');
        $this->resource($name, '__fields');

        $this->info('appending route...');
        File::append(base_path('routes/web.php'), 'Route::resource(\'' . Str::plural(Str::lower($name)) . "', '{$name}Controller');");

    }


    protected function model($name)
    {
        $content = str_replace(['{{modelSU}}'], [$name], $this->getStub('model'));
        file_put_contents(app_path("/{$name}.php"), $content);
    }

    protected function datatable($name)
    {
        $content = str_replace(['{{modelPU}}'], [Str::plural(Str::ucfirst($name))], $this->getStub('datatable'));
        file_put_contents(app_path("DataTables/".Str::plural(Str::ucfirst($name)) ."DataTable.php"), $content);
    }

    protected function controller($name)
    {
        $content = str_replace(
            [
                '{{modelSU}}',
                '{{modelPL}}',
                '{{modelSL}}',
                '{{modelPU}}',
            ],
            [
                $name,
                Str::plural(Str::lower($name)),
                Str::singular(Str::lower($name)),
                Str::plural(Str::ucfirst($name))
            ],
            $this->getStub('controller')
        );

        file_put_contents(app_path("/Http/Controllers/{$name}Controller.php"), $content);
    }

    protected function resource($name, $crudName)
    {
        $content = str_replace(
            [
                '{{modelSU}}',
                '{{modelPL}}',
                '{{modelSL}}',
                '{{modelPU}}',
            ],
            [
                $name,
                Str::plural(Str::lower($name)),
                Str::singular(Str::lower($name)),
                Str::plural(Str::ucfirst($name))
            ],
            $this->getStub($crudName)
        );
        if (!is_dir(resource_path("views/" . Str::lower(Str::plural($name)))))
            mkdir(resource_path("views/" . Str::lower(Str::plural($name))), 0777, true);
        file_put_contents(resource_path("views/" . strtolower(Str::plural($name)) . "/$crudName.blade.php"), $content);
    }

    protected function getStub($type)
    {
        return file_get_contents(base_path("CubeMaster/stubs/$type.stub"));
    }
}
