<?php

namespace App\DataTables;

use CubeMaster\Traits\DataTableTrait;
use Yajra\DataTables\Services\DataTable;

class HallsDataTable extends DataTable
{
    use DataTableTrait;

    protected function getColumns()
    {

        return [

          'name'=> ['title' => __('name')],
            'price'=> ['title' => __('price')],
          'created_at'=> ['title' => __('created_at')],
          'updated_at'=> ['title' => __('updated_at')],
        ];

    }

}
