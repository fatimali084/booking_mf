<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hall extends Model
{
        public function hall()
    {
        $this->hasMany('App\Service');
    }

      protected $fillable = [
        'name', 'price',
    ];
    }
