<?php

namespace App\Http\Controllers;

use App\Service;
use App\DataTables\ServicesDataTable;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $dataTable = new ServicesDataTable(Service::newModelInstance());
        return $dataTable->render('services.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $service = new Service;
        return view('services.create', compact('service'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|unique:services',
        ]);

        $service = Service::create($request->all());
        return redirect('services')->withMessage(__('added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        return view('services.edit', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Service $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {

        $this->validate($request, [
            'name' => 'required|unique:services',
        ]);

        $service->update($request->all());
        return redirect('services')->withMessage(__('updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();
        return redirect('services')->withMessage(__('deleted_successfully'));
    }
}
