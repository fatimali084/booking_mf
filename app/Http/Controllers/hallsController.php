<?php

namespace App\Http\Controllers;

use App\Hall;
use App\Service;
use App\DataTables\HallsDataTable;
use Illuminate\Http\Request;

class hallsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $dataTable = new HallsDataTable(Hall::newModelInstance());
        return $dataTable->render('halls.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $hall = new Hall;
        return view('halls.create', compact('hall'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'name' => 'required|unique:halls',
         ]);

        $hall = Hall::create($request->all());

    //    if($request->ajax())
      //  {
        //    $rules = array(
          //      'name.*'  => 'required',
            //    'type.*'  => 'required'
           // );
            //$error = Validator::make($request->all(), $rules);
           // if($error->fails())
           // {
             //   return response()->json([
             //       'error'  => $error->errors()->all()
             //   ]);
            //}
            $service=new Service;
            $service2 =array();
            $service->halls_id=$hall->id;
            $service->name=$request->input('name');
           // $name=$request->input('name');
            $service->type=$request->input('type');
           // $type=$request->input('type');
            //for($count = 0; $count < count($service); $count++)
            //{
              //  $data = array(
                //    'name' => $name[$count],
                  //  'type'  => $type[$count]
                //);
               // $insert_data[] = $data;
           // }
          //  Service::insert($insert_data);
          //  return response()->json([
            //    'success'  => 'Data Added successfully.'
           // ]);
        //}



        $service->save();

        return redirect('halls')->withMessage(__('added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\halls $hall
     * @return \Illuminate\Http\Response
     */
    public function show(Hall $hall)
    {
        return view('halls.edit', compact('hall'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\halls $hall
     * @return \Illuminate\Http\Response
     */
    public function edit(Hall $hall)
    {
        return view('halls.edit', compact('hall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\halls $hall
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hall $hall)
    {

        $this->validate($request, [
            'name' => 'required|unique:halls',
        ]);

        $hall->update($request->all());
        return redirect('halls')->withMessage(__('updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\halls $hall
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hall $hall)
    {
        $hall->delete();
        return redirect('halls')->withMessage(__('deleted_successfully'));
    }
}
