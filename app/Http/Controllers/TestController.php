<?php

namespace App\Http\Controllers;

use App\Test;
use App\DataTables\TestsDataTable;
use Illuminate\Http\Request;

class TestController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $dataTable = new TestsDataTable(Test::newModelInstance());
        return $dataTable->render('tests.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $test = new Test;
        return view('tests.create', compact('test'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|unique:tests',
        ]);

        $test = Test::create($request->all());
        return redirect('tests')->withMessage(__('added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Test $test
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test)
    {
        return view('tests.edit', compact('test'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Test $test
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test)
    {
        return view('tests.edit', compact('test'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Test $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Test $test)
    {

        $this->validate($request, [
            'name' => 'required|unique:tests',
        ]);

        $test->update($request->all());
        return redirect('tests')->withMessage(__('updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Test $test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test $test)
    {
        $test->delete();
        return redirect('tests')->withMessage(__('deleted_successfully'));
    }
}
