<?php

namespace CubeMaster\Traits;

trait DataTableTrait
{

    public $model;

    function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function ($model) {
                return '<a href="/' . $model->getTable() . '/' . $model->id . '" title="' . __('show') . '" class="btn btn-info">' . __('show') . '
									<i class="la la-eye"></i>
								</a>						
								<a id="btn-delete" data-action="/' . $model->getTable() . '/' . $model->id . '"  title="' . __('delete') . '" class="btn btn-danger">' . __('delete') . '
									<i class="la la-ban"></i>
								</a>';

            });

    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_keys($this->getColumns()))
            ->minifiedAjax()
            ->addAction(['title' => __('action')])
            ->parameters($this->parameters());
    }


    /**
     * Get query source of dataTable.
     *
     * @param \App\Category $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return $this->model->newQuery()->newQuery()->select(array_keys($this->getColumns()));
    }

    public function parameters()
    {
        return [
            'dom' => "<<'d-flex justify-content-center'f><'col-sm-12 col-md-6'>>" .
                "<'row'<'col-sm-12'tr>>" .
                "<'row'<'col-sm-12 col-md-4'i><'col-sm-12 col-md-2'l><'col-sm-12 col-md-6'p>>",
            'order' => [[0, 'desc']],
        ];
    }


}

?>
