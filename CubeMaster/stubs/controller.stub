<?php

namespace App\Http\Controllers;

use App\{{modelSU}};
use App\DataTables\{{modelPU}}DataTable;
use Illuminate\Http\Request;

class {{modelSU}}Controller extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $dataTable = new {{modelPU}}DataTable({{modelSU}}::newModelInstance());
        return $dataTable->render('{{modelPL}}.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        ${{modelSL}} = new {{modelSU}};
        return view('{{modelPL}}.create', compact('{{modelSL}}'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|unique:{{modelPL}}',
        ]);

        ${{modelSL}} = {{modelSU}}::create($request->all());
        return redirect('{{modelPL}}')->withMessage(__('added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\{{modelSU}} ${{modelSL}}
     * @return \Illuminate\Http\Response
     */
    public function show({{modelSU}} ${{modelSL}})
    {
        return view('{{modelPL}}.edit', compact('{{modelSL}}'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\{{modelSU}} ${{modelSL}}
     * @return \Illuminate\Http\Response
     */
    public function edit({{modelSU}} ${{modelSL}})
    {
        return view('{{modelPL}}.edit', compact('{{modelSL}}'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\{{modelSU}} ${{modelSL}}
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, {{modelSU}} ${{modelSL}})
    {

        $this->validate($request, [
            'name' => 'required|unique:{{modelPL}}',
        ]);

        ${{modelSL}}->update($request->all());
        return redirect('{{modelPL}}')->withMessage(__('updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\{{modelSU}} ${{modelSL}}
     * @return \Illuminate\Http\Response
     */
    public function destroy({{modelSU}} ${{modelSL}})
    {
        ${{modelSL}}->delete();
        return redirect('{{modelPL}}')->withMessage(__('deleted_successfully'));
    }
}
