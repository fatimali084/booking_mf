@extends('layouts.app')

@section('content')
@include('includes.feedback-messages')
    <div class="col-md-7 my-5">
        <h1>{{__('edit').' '.__('services')}}</h1>
        <form action="/services/{{$services->id}}" method="post">
            @csrf
            @method('PATCH')
            @include('services.__fields')
            <button class="btn btn-info">{{__('save')}}</button>
        </form>
    </div>
@endsection