@extends('layouts.app')

@section('content')
    @include('includes.feedback-messages')
    <div class="row">
        <div class="col-md-12">
            <a href="/services/create" class="btn btn-info">{{__('create')}}</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! $dataTable->table() !!}
        </div>
    </div>
    @include('includes.datatable')
    @push('after_scripts')
    @include('includes.delete-form')
    @endpush
@endsection
