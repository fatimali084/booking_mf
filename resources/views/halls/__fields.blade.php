<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<div class="form-group">
    <label id="nameLbl" for="name"> {{__('أسم القاعة')}}: </label>
    <input
            class="form-control @if($errors->has("name")) is-invalid @endif"
            id="name" type="text" name="name"
            placeholder="{{__("أسم القاعة")}}" value='{{old("name",$hall->name)}}'
            @if(isset($required)) required @endif
    />
    <div class="invalid-feedback">
        {{$errors->first("name")?:__('الرجاء أدخال أسم القاعة ').__("name")}}
    </div>





    <div class="form-inline" id="name">

        <div class="table-responsive">
            <form method="post" id="dynamic_form">
                <span id="result"></span>
                <table class="table table-bordered table-striped" id="user_table">
                    <thead>
                    <tr>
                        <th width="35%">الخدمات</th>
                        <th width="35%">النوع</th>
                        <th width="30%"></th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </form>
        </div>
        <div class="form-group " id="serv">

            <label id="nameLbl" for="name" > {{__("الخدمات")}}: </label>
            <input
                    class="form-control  m-4  @if($errors->has("name")) is-invalid @endif"
                    id="name" type="text" name="addmore[0][name]"
                    placeholder="{{__("الخدمات")}}" value="{{old("name",$hall->name)}}"
                    @if(isset($required)) required @endif
            />


            <div class="invalid-feedback">
                {{$errors->first("name")?:__('الرجاء أدخال الخدمات').__("name")}}
            </div>


            <label id="nameLbl" for="name"> {{__('النوع')}}: </label>
            <select class=" m-4" name="type">
                class="form-control @if($errors->has("name")) is-invalid @endif"
                id="name" type="text" name="addmore[0][serv]"
                placeholder="{{__("النوع")}}" value='{{old("name",$hall->type)}}'
                @if(isset($required)) required @endif
                <option value="مجانا">مجانا</option>
                <option value="سعر ثابت">سعر ثابت </option>
                <option value="لكل وحدة">لكل وحدة</option>
            </select>


            <div class="invalid-feedback">
                {{$errors->first("name")?:__('الرجاء أدخال نوع الخدمة').__("name")}}
            </div>
            <button class="btn btn-xs btn-danger btn-add-more-sarve m-4" name="remove-tr">{{__(" حذف")}}</button>
            <a class="btn btn-xs btn-info btn-add-more-sarve" >{{__('إضافه')}}</a>
            <button type="button" name="add" id="add" class="btn btn-success">Add More</button><br>

        </div>

    </div>



    <label id="nameLbl" for="name"> {{__('السعر الأفتراضي')}}: </label>
    <input
            class="form-control @if($errors->has("name")) is-invalid @endif"
            id="name" type="text" name="price"
            placeholder="{{__("السعر الأفتراضي")}}" value='{{old("name",$hall->price)}}'
            @if(isset($required)) required @endif
    />
    <div class="invalid-feedback">
        {{$errors->first("name")?:__('الرجاء أدخال السعر الأفتراضي').__("name")}}
    </div>



</div>

<script type="text/javascript">

    var i = 0;

    $("#add").click(function(){

        ++i;

        $("#serv").append(

            '<label id="nameLbl" for="name"> {{__("الخدمات")}}: </label> <input class="form-control  m-4  @if($errors->has("name")) is-invalid @endif" id="name" type="text" name="addmore['+i+'][name]" placeholder="{{__("الخدمات")}}" value="{{old("name",$hall->name)}}" @if(isset($required)) required @endif/>    <label id="nameLbl" for="name"> {{__("النوع")}}: </label><select class=" m-4">class="form-control @if($errors->has("name")) is-invalid @endif"id="name" type="text" name="addmore['+i+'][serv]"placeholder="{{__("النوع")}}" value="{{old("name",$hall->name)}}"@if(isset($required)) required @endif<option value="مجانا">مجانا</option><option value="سعر ثابت">سعر ثابت </option><option value="لكل وحدة">لكل وحدة</option></select><br><button class="btn btn-xs btn-danger btn-add-more-sarve m-4" name="remove-tr">{{__(" حذف")}}</button><br>'

        );
// $("#serv").appendTo('#name');
    });

       $(document).on('click', '.remove-tr', function(){
            $(this).parents('tr').remove();
        });

</script>
<!--
<script>
    $(document).ready(function(){

        var count = 1;

        dynamic_field(count);

        function dynamic_field(number)
        {
            html = '<tr>';
            html += '<td><input type="text" name="name[]" class="form-control" /></td>';
            html += '<td><select  class="form-control" name="type[]"><option value="مجانا">مجانا</option>'+
                '                <option value="سعر ثابت">سعر ثابت </option>' +
                '                <option value="لكل وحدة">لكل وحدة</option>' +
                '            </select></td>';
            if(number > 1)
            {
                html += '<td><button type="button" name="remove" id="" class="btn btn-danger remove">Remove</button></td></tr>';
                $('tbody').append(html);
            }
            else
            {
                html += '<td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td></tr>';
                $('tbody').html(html);
            }
        }

        $(document).on('click', '#add', function(){
            count++;
            dynamic_field(count);
        });

        $(document).on('click', '.remove', function(){
            count--;
            $(this).closest("tr").remove();
        });


    });
</script>-->
