@extends('layouts.app')

@section('content')
@include('includes.feedback-messages')

<div class="col-md-7 my-5">
    <h1>{{__('إضافة').' '.__('قاعة')}}</h1>
    <form  action="/halls" method="post" enctype="multipart/form-data">
        @csrf
        @include('halls.__fields')
        <button class="btn btn-info">{{__('إضافه')}}</button>
    </form>
</div>
@endsection