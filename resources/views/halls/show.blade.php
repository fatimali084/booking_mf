@extends('layouts.app')

@section('content')
@include('includes.feedback-messages')
    <div class="col-md-7 my-5">
        <h1>{{__('edit').' '.__('halls')}}</h1>
        <form action="/halls/{{$halls->id}}" method="post">
            @csrf
            @method('PATCH')
            @include('halls.__fields')
            <button class="btn btn-info">{{__('save')}}</button>
        </form>
    </div>
@endsection