<!DOCTYPE html>
<html lang="{{ app()->getLocale()}}" dir="{{app()->getLocale()=='ar'?'rtl':'ltr'}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="{{$description??''}}">
    <meta name="author" content="{{$author??'CubeMaster'}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$title??'CubeMaster'}}</title>
    <meta name="keyword" content="{{$keywords??''}}">
    @stack('before_styles')
    <link rel="stylesheet" href="https://unpkg.com/@coreui/coreui/dist/css/coreui.min.css">
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    @stack('after_styles')
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
@include('layouts._navbar')
<div class="app-body">
    @include('layouts._sidebar')
    <main class="main">
        <!-- Breadcrumb-->
        @include('layouts._breadcrumb')
        <div class="container-fluid">
            <div class="animated fadeIn"></div>
            @yield('content')
        </div>
    </main>
</div>
@include('layouts._footer')
@stack('before_scripts')
{{--<script src="{{ asset('js/app.js') }}"></script>--}}
<script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script src="https://unpkg.com/@coreui/coreui/dist/js/coreui.min.js"></script>
@stack('after_scripts')
</body>
</html>
