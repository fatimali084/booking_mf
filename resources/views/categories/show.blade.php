@extends('layouts.app')

@section('content')
@include('includes.feedback-messages')
    <div class="col-md-7 my-5">
        <h1>{{__('edit').' '.__('categories')}}</h1>
        <form action="/categories/{{$categories->id}}" method="post">
            @csrf
            @method('PATCH')
            @include('categories.__fields')
            <button class="btn btn-info">{{__('save')}}</button>
        </form>
    </div>
@endsection