@extends('layouts.app')

@section('content')
@include('includes.feedback-messages')
    <div class="col-md-7 my-5">
        <h1>{{__('edit').' '.__('category')}}</h1>
        <form action="/categories/{{$category->id}}" method="post">
            @csrf
            @method('PATCH')
            @include('categories.__fields')
            <button class="btn btn-info">{{__('edit')}}</button>
        </form>
    </div>
@endsection