@extends('layouts.app')

@section('content')
@include('includes.feedback-messages')

<div class="col-md-7 my-5">
    <h1>{{__('add').' '.__('category')}}</h1>
    <form  action="/categories" method="post" enctype="multipart/form-data">
        @csrf
        @include('categories.__fields')
        <button class="btn btn-info">{{__('create')}}</button>
    </form>
</div>
@endsection