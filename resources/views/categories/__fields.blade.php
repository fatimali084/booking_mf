<div class="form-group">
    <label id="nameLbl" for="name"> {{__('name')}}: </label>
    <input
            class="form-control @if($errors->has("name")) is-invalid @endif"
            id="name" type="text" name="name"
            placeholder="{{__("name")}}" value='{{old("name",$category->name)}}'
            @if(isset($required)) required @endif
    />
    <div class="invalid-feedback">
        {{$errors->first("name")?:__('Please provide a valid ').__("name")}}
    </div>
</div>
