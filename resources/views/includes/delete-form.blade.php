<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script>

    $(function(){
$(document).on("click", '#btn-delete', function (e) {
   var deleteButton = this;
    e.preventDefault();
    swal.setDefaults({
    });
    swal("تأكيد عملية الحذف", {
    icon: "error",
    dangerMode: true,
        buttons: ["إلغاء", "احذف"],
        confirmButtonColor: '#DD6B55',
}).then(function(isConfirm) {
        if (isConfirm) {
            $('#form-delete').attr('action', $(deleteButton).data('action'));
            $('#form-delete').submit();
        }
    });
});
});
</script>


<form id="form-delete" action="" method="post">
    @csrf
    @method('delete')
</form>
