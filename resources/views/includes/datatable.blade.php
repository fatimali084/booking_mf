@push('after_styles')

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"/>

<style>
    .dataTables_filter input { width: 800px }

    .dataTable thead {
        background-color: #198ff0;
        color: white;
    }
    #dataTableBuilder_previous .page-link,#dataTableBuilder_next .page-link{
        border-radius: 0 !important;
    }
    /*.disabled .page-link:hover{*/
    /*background-color: #ebe9f2 !important;*/
    /*}*/
    .dataTables_wrapper .pagination .page-item.disabled:hover>.page-link {
        background-color: #ebe9f2 !important;
    }
</style>

@endpush
@push('after_scripts')
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
{{--<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">--}}
{{--<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>--}}
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
<script>
    $(document).ready(function () {
        $('.dataTables_filter input[type="search"]').css(
            {'width':'350px','display':'inline-block'}
        );
    });
    $.extend(true, $.fn.dataTable.defaults, {
        language: {

            "sProcessing": "جارٍ التحميل...",
            "sLengthMenu": "أظهر _MENU_ مدخلات",
            "sZeroRecords": "لم يعثر على أية سجلات",
            "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
            "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
            "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
            "sSearch": "ابحث:",
            "oPaginate": {
                "sFirst": "الأول",
                "sPrevious": "السابق",
                "sNext": "التالي",
                "sLast": "الأخير"
            },
        },}
    );

</script>

{!! $dataTable->scripts() !!}

@endpush

