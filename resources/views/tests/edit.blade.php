@extends('layouts.app')

@section('content')
@include('includes.feedback-messages')
    <div class="col-md-7 my-5">
        <h1>{{__('edit').' '.__('test')}}</h1>
        <form action="/tests/{{$test->id}}" method="post">
            @csrf
            @method('PATCH')
            @include('tests.__fields')
            <button class="btn btn-info">{{__('edit')}}</button>
        </form>
    </div>
@endsection