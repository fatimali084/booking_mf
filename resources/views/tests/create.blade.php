@extends('layouts.app')

@section('content')
@include('includes.feedback-messages')

<div class="col-md-7 my-5">
    <h1>{{__('add').' '.__('test')}}</h1>
    <form  action="/tests" method="post" enctype="multipart/form-data">
        @csrf
        @include('tests.__fields')
        <button class="btn btn-info">{{__('create')}}</button>
    </form>
</div>
@endsection